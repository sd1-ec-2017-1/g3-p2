var express = require('express');
var app = express();
var server = require('http').Server(app);
var socketio = require('socket.io')(server);
var cookieParser = require('cookie-parser');
var cookiepar = require('cookieparser');

var bodyParser = require('body-parser');  // processa corpo de requests
var irc = require('irc');
var path = require('path');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

var nicks = [];

socketio.listen(server).on('connection', function (socket) {

	var req = socket.client.request.headers.cookie;
	var cookie = cookiepar.parse(req);
	var servidor = cookie.servidor;
	var nick = cookie.nick;
	var canal = cookie.canal;

  var client = new irc.Client( 	
    servidor,
		nick,
		{ channels: [ canal ],}
  );

	nicks.push(nick);
	
	socket.on('myData', function() {
	  socket.emit('myData', nick, servidor);
	});

  socket.on('message', function (canal, msg) {
		console.log('Message Received: ', msg);

		var words = msg.split(' ');
		if(words[0] == "/msg") {
			concatMsg = "";
			for(var i = 2; i < words.length; i++) {
				concatMsg = concatMsg + words[i] + " ";
			}
			client.ctcp(words[1], "privmsg", concatMsg);
		}
		else if(words[0] == "/join") {
		  concatMsg = "";
			for(var i = 1; i < words.length; i++) {
				concatMsg = concatMsg + words[i] + " ";
			}
			client.join(concatMsg);
		}
		else if(words[0] == "/part") {
		  concatMsg = "";
			for(var i = 2; i < words.length; i++) {
				concatMsg = concatMsg + words[i] + " ";
			}
			client.part(words[1], concatMsg);
		}
		else {
			client.say(canal, msg);
		}
		
	});

	client.addListener('ctcp-privmsg', function(from, to, text, message) {
	  socket.emit('message', to, "privmsg from " + from + ": " + text);
  });
  
  client.addListener('join', function (channel, nick) {
    socket.emit('join', channel, nick, ' --> ' + nick + ' entrou no canal.');
    console.log(channel + ' => ' + nick + ' entrou no canal.');
  });
  
  client.addListener('part', function(channel, nick, reason) {
    if(reason) {
      socket.emit('part', channel, nick, ' --> ' + nick + ' saiu no canal: ' + reason);
      console.log(channel + ' => ' + nick + ' saiu no canal: ' + reason);
    }
    else {
      socket.emit('part', channel, nick, ' --> ' + nick + ' saiu no canal');
      console.log(channel + ' => ' + nick + ' saiu no canal');    
    }
  });
  
  client.addListener('message#', function (from, to, message) {  // to = canal para o qual a mensagem foi enviada
    socket.emit('message', to, from + ' => '+ to +': ' + message);
    console.log(from + ' => '+ to +': ' + message);
  });

	client.addListener('names', function(channel, nicks) {
		socket.emit('nicksDoCanal', channel, nicks);
	});

  client.addListener('motd', function(motd) {
    socket.emit('motd',motd);
  });
  
  client.addListener('error', function(message) {
    console.log('error: ', message);
	});
	
	client.addListener('selfMessage', function(to, message){
	  socket.emit('message', to, message);
	});

	socket.on('disconnect', function () {
		var i = nicks.indexOf(nick);
		nicks.splice(i,1);

		client.disconnect();
	});
});

app.get('/', function (req, res) {
  if ( !(req.cookies.used == "true") && req.cookies.servidor && req.cookies.nick  && req.cookies.canal && nicks.indexOf(req.cookies.nick) == -1) {
	res.cookie('used',true);
  	res.sendFile(path.join(__dirname, '/index.html'));
  }
  else {
    res.sendFile(path.join(__dirname, '/login.html'));
  }
});

app.post("/login", function (req, res) {
	res.cookie('nick', req.body.nick);
	res.cookie('canal', req.body.canal);
	res.cookie('servidor', req.body.servidor);
	res.cookie('used',false);
	res.redirect('/');
});

server.listen(3000, function(){
  console.log('listening on: *:3000');
});
