var iosocket;
var myNick;
var myServer;
var canais = {};
var canalId = 0;
var canalAtivo = "";

$(document).ready(function () {
  iosocket = io.connect();

  iosocket.on('connect', function () {
    iosocket.emit('myData');
  
    iosocket.on('myData', function(nick, server) {
      myNick = nick;
      myServer = server;
      criarCanal(myServer);
    });
    
    iosocket.on('join', function(canal, nick, message) {
      if(nick == myNick) {
        criarCanal(canal, true);
      }
      else {
        $('#nicks'.concat(canais[canal])).append($('<li></li>').text(nick));
      }
      adicionarMensagem(canal, message);
    });
    
    iosocket.on('part', function(canal, nick, message) {
      if(nick == myNick) {
        deletarCanal(canal);
      }
      else {
        $('#nicks'.concat(canais[canal])).children("li").filter(":contains("+ nick +")").remove();
        adicionarMensagem(canal, message);
      }
    });
    
    iosocket.on('message', function(to, message) {  // to = canal
      if(canais.hasOwnProperty(to)) adicionarMensagem(to, message);
      else adicionarMensagem(myServer, message);
    });
    
    iosocket.on('nicksDoCanal', function(canal, nicks) {
      for(var nick in nicks) {
        $('#nicks'.concat(canais[canal])).append($('<li></li>').text(nicks[nick] + "   " +nick));
      }
		});

    iosocket.on('motd', function(message) {
      adicionarMensagem(myServer, message);
		});

    iosocket.on('disconnect', function() {
      for(var canal in canais) {
        adicionarMensagem(canal, "Disconnected");
      }
    });
  });

  $('#butao').click(function() {enviarMensagem();});
  
  $('#mensagem').keypress(function(event) {
    if(event.which == 13){
      event.preventDefault();
      enviarMensagem();
    }  
  });
});

function adicionarMensagem(canal, msg) {
  var muralId = '#mural'.concat(canais[canal]);
  $(muralId).append($('<li></li>').text(msg));
  $(muralId).scrollTop($(muralId).prop('scrollHeight'));
}

function criarCanal(canal, usaNicks) {
  var cId = canalId.toString();
  canais[canal] = cId;
  
  var novo_botaoCanal = $('<div class="botaoCanal"></div>');
  novo_botaoCanal.append($('<span id="botao'+cId+'"></span>').text(canal));
  novo_botaoCanal.click(function(){exibirCanal($(this));});
  $('#salas').append(novo_botaoCanal);
  
  var novo_mural = $('<div class="mural" id="mural'+cId+'"></div>');
  $('#chat').append(novo_mural);
  
  if(usaNicks) {
    var novo_nicksDoCanal = $('<div class="nicksNoCanal" id="nicks'+cId+'"></div>');
    novo_nicksDoCanal.append($('<h3></h3>').text("Nicks no canal:"));
    $('#chat').append(novo_nicksDoCanal);
  }
  
  canalId++;
  
  exibirCanal($('#botao'.concat(cId)));
}

function deletarCanal(canal) {
  var cId = canais[canal];
  
  if(canal == canalAtivo) {
    exibirCanal($('#botao0'));  
  }
  
  $('#botao'.concat(cId)).parent().remove();
  $('#mural'.concat(cId)).remove();
  $('#nicks'.concat(cId)).remove();
}

function enviarMensagem() {
  iosocket.send(canalAtivo, $('#mensagem').val());
  $('#mensagem').val('');
}

function exibirCanal(elemento) {
  // Escondendo o canal que até então estava ativo
  if(canalAtivo != "") {
    var cId = canais[canalAtivo];
    $('#mural'.concat(cId)).toggle();

    if($('#nicks'.concat(cId))) {
      $('#nicks'.concat(cId)).toggle();
    }
  }

  // Tornando visível o canal que se deseja exibir
  cId = canais[elemento.text()];
  $('#mural'.concat(cId)).toggle();

  if($('#nicks'.concat(cId))) {
    $('#nicks'.concat(cId)).toggle();
  }
  
  canalAtivo = elemento.text();
}
